angular.module('carsManager').directive('carModel', ['Models', function (Models) {
    var Controller = ["$rootScope", "$scope", "Models", function ($rootScope, $scope, Models) {
        $scope.models = Models.query();
        Models.get({id:$scope.mod}, function (res, headers) {
            $scope.modelName = res.name;
        });
    }];

    return{
        restrict: 'AE',
        templateUrl: "directives/car-model.html",
        scope: {
            mod: "="
        },
        controller: Controller
    }
}]);