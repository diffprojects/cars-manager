angular.module('carsManager').directive('carsModelsListDirective', ['Models', function (Models) {
    var Controller = ["$rootScope", "$scope", "Models", function ($rootScope, $scope, Models) {
        $scope.carModels = Models.query();

        $scope.updateModel = function(val){
            $scope.car.carmodel = val;
        }
    }];

    return{
        restrict: 'AE',
        templateUrl: "directives/cars-models-list-directive.html",
        controller: Controller
    }
}]);