var carsManager = angular.module('carsManager', ['ngRoute', 'ngResource', 'ui.bootstrap', 'ui.bootstrap.tpls', 'angularFileUpload', 'lr.upload', 'ui.date']);

carsManager.config(['$routeProvider', '$locationProvider', function ($routeProvide, $locationProvider) {
  $routeProvide
    .when('/', {
      templateUrl: 'templates/home.html',
      controller: 'HomeController'
    })
    .when('/cars', {
      templateUrl: 'templates/cars.html',
      controller: 'CarsListController'
    })
    .when('/carmodels', {
      templateUrl: 'templates/car-models.html',
      controller: 'CarModelsController'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);

/*Factory*/
carsManager.factory('Cars', [
  '$resource', function ($resource) {
    return $resource(
        'http://localhost:3000/cars/:id', // url
        {id:'@id'},                        // params
        {
          update: {
            method: 'PUT'
          },
          save: {
            method: 'POST'
          },
          remove: {
            method: 'DELETE'
          }
        }
    );
  }
]);
carsManager.factory('Models', [
  '$resource', function ($resource) {
    return $resource(
        'http://localhost:3000/models/:id', // url
        {id:'@id'},                        // params
        {
          update: {
            method: 'PUT'
          },
          save: {
            method: 'POST'
          },
          remove: {
            method: 'DELETE'
          }
        }
    );
  }
]);

carsManager.controller('CarsListController', ['$scope', 'Cars', '$uibModal', function ($scope, Cars, $uibModal) {
  $scope.cars = Cars.query();

  $scope.open = function (car) {
    $scope.car = car;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'templates/car-modal.html',
      controller: 'ModalCarCtrl',
      scope: $scope
    });
  }

  $scope.addCar = function () {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'templates/car-modal.html',
      controller: 'ModalNewCarCtrl',
      scope: $scope
    }).result.then(function(item) {
      $scope.cars.push(item);
    });
  }
  
  $scope.remove = function (car, ind) {

    $scope.car = car;
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'templates/car-confirm-delete.html',
      controller: 'ModalCarDeleteCtrl',
      scope: $scope
    }).result.then(function() {
      $scope.cars.splice(ind, 1);
    });
  }

  $scope.sortedField = undefined;
  $scope.reverse = false;
  $scope.sortByFieldName = function (fieldName) {
    if($scope.sortedField === fieldName){
      $scope.reverse = !$scope.reverse;
    }else {
      $scope.sortedField = fieldName;
      $scope.reverse = false;
    }
  }

}]);

carsManager.controller('ModalCarCtrl', ['$scope', '$uibModalInstance', 'Cars', 'FileUploader', function ($scope, $uibModalInstance, Cars, FileUploader) {

  $scope.uploader = new FileUploader();

  $scope.ok = function (car) {
    var db_car = Cars.get({id:$scope.car.id}, function () {

      db_car.name = $scope.car.name;
      db_car.carmodel = $scope.car.carmodel;
      db_car.created = $scope.car.created;
      db_car.image = $scope.car.image;

      db_car.$update($scope.car);
    });

    $uibModalInstance.close();
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);

carsManager.controller('ModalNewCarCtrl', ['$scope', '$uibModalInstance', 'Cars', 'FileUploader', function ($scope, $uibModalInstance, Cars, FileUploader) {

  $scope.uploader = new FileUploader();
  $scope.uploader.autoUpload = true;
  $scope.uploader.url = "http://localhost:8000/uploads";



  $scope.uploader.onAfterAddingFile = function(item) {
    console.log(item);
  };
  $scope.uploader.onSuccessItem = function(item, response, status, headers) {
    console.log(item);
    console.log(response);
    console.log(status);
    console.log(headers);
  };
  $scope.uploader.onErrorItem = function(item, response, status, headers) {};

  //$scope.options

  $scope.ok = function () {

    new_car = new Cars();
    new_car.name = $scope.car.name;
    new_car.carmodel = $scope.car.carmodel;
    new_car.created = $scope.car.created;
    new_car.image = $scope.car.image;

    new_car.$save(function (msg, headers) {
      $scope.car.id = msg.id;
    });

    $uibModalInstance.close($scope.car);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);

carsManager.controller('ModalCarDeleteCtrl', ['$scope', '$uibModalInstance', 'Cars', function ($scope, $uibModalInstance, Cars) {
  $scope.ok = function () {
    del_car = new Cars();
    del_car.name = $scope.car.name;
    del_car.carmodel = $scope.car.carmodel;
    del_car.created = $scope.car.created;
    del_car.image = $scope.car.image;

    del_car.$delete({id:$scope.car.id});

    $uibModalInstance.close();

    return $scope.ind;
  };

  $scope.reject = function () {
    $uibModalInstance.dismiss('cancel');
  };
}]);

carsManager.controller('HomeController', ['$scope', '$http', '$location', function ($scope, $http, $location) {

}]);

carsManager.controller('CarModelsController', ['$scope', '$http', '$location', 'Models', function ($scope, $http, $location, Models) {
  $scope.carmodels = Models.query();

  $scope.changeName = function (carmodel) {
    $scope.carmodel = carmodel;
    var db_carmod = Models.get({id:$scope.carmodel.id}, function () {
      db_carmod.name = $scope.carmodel.name;
      db_carmod.$update($scope.carmodel);
    });
  }

  $scope.addModel = function (new_carmodel) {
    carmodel = new Models();
    carmodel.name = $scope.new_carmodel.name;

    carmodel.$save(function (msg, headers) {
      $scope.new_carmodel.id = msg.id;
    }).then(function(item){
      $scope.carmodels.push(item);
      $scope.new_carmodel.name = null;
    });
  }

  $scope.removeCarmodel = function (carmodel, ind) {

    $scope.carmodel = carmodel;

    del_carmodel = new Models();
    del_carmodel.name = $scope.carmodel.name;

    del_carmodel.$delete({id:$scope.carmodel.id}).then(function() {
      $scope.carmodels.splice(ind, 1);
    });

  }
}]);