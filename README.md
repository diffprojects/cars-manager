# README for Cars Manager project #

This README would normally document whatever steps are necessary to get your application up and running.

### Preparing the system ###

1. It is necessary to install nodejs [For Ubuntu users](https://www.digitalocean.com/community/tutorials/node-js-ubuntu-14-04-ru)
2. Then install the REST server, in this project this is JSON-server [Install JSON-server](https://github.com/typicode/json-server)

## Install bower components ##
1. Open the /angular-application directory in terminal window
2. run 
```
#!bash

bower install
```

### Using the project ###

1. Open the /rest-server directory of project in terminal
2. Run the command
```
#!bash

json-server cars.json
```

3. Open the /angular-application directory in another terminal window 
4. Run the command


```
#!bash

npm start
```
or if not working


```
#!bash

sudo npm start
```

4. Open http://localhost:8000/ in your browser
5. Also you can control data if open the [http://localhost:3000/](http://localhost:3000/)